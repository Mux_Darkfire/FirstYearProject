from ROOT import TH1D, TF1, TAttLine, TMath, gStyle, kRed, kBlue, TH1F

def mass_fit(histo):
    """
    Fit masses to gaussian + straight line
    """

    mass_fit1 = TF1("mass_fit1", "gaus(0)",0.485,0.51)
    mass_fit2 = TF1("mass_fit2", "[0]*x+[1]",0.3,0.7)

    histo.Fit("mass_fit1", "LR")
    fitMR1 = histo.GetFunction("mass_fit1")
    norm = fitMR1.GetParameter(0)
    mean = fitMR1.GetParameter(1)
    sigma = fitMR1.GetParameter(2)

    histo.Fit("mass_fit2", "LR")
    fitMR2 = histo.GetFunction("mass_fit2")
    a = fitMR2.GetParameter(0)
    b = fitMR2.GetParameter(1)

    def format_line(line, color, style):
        line.SetLineWidth(2)
        line.SetLineColor(color)
        line.SetLineStyle(style)

    def the_gausppar(variables, parameters):
        return parameters[0]*TMath.Gaus(variables[0], parameters[1],parameters[2]) + parameters[3] + parameters[4]*variables[0]

    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    gStyle.SetOptFit(1111)
    gStyle.SetStatBorderSize(0)
    gStyle.SetStatX(.89)
    gStyle.SetStatY(.89)

    parabola = TF1("parabola", "[0]+[1]*x", 0.35, 0.7)
    format_line(parabola, kBlue, 2)
    gaussian = TF1("gaussian", "[0]*TMath::Gaus(x,[1],[2])", 0.485, 0.51)
    format_line(parabola, kRed, 2)
    gausppar = TF1("gausppar", the_gausppar, 0.35, 0.7, 5)

    gausppar.SetParameters(norm, mean, sigma, a, b)
    gausppar.SetParNames("Norm", "Mean", "Sigma", "a", "b")
    format_line(gausppar, kBlue, 1)

    histo.SetMarkerStyle(8)

    gausppar.SetParameter(0, 1500)
    gausppar.SetParameter(1, 0.48)
    npar = gausppar.GetNpar()
    for ipar in range(3, npar):
        gausppar.SetParameter(ipar, 1)

    fitRes = histo.Fit(gausppar, "S")
    fitRes.Print()

    for ipar in range(3):
        gaussian.SetParameter(ipar, gausppar.GetParameter(ipar))
        parabola.SetParameter(ipar, gausppar.GetParameter(ipar+3))

    histo.DrawClone("PE")
    parabola.DrawClone("Same")
    gaussian.DrawClone("Same")


def lifetime_fit(histo):
    """
    Fit lifetimes
    """

    # expo(0) is apparently a function for fitting exponentials ( exp([0] + x*[1] )
    # The output will be 1/tau according to Vojtech
    # parameter 0 is fixed to 0. It seemed to provide better results

    expfit = TF1("expfit", "expo(0)", 0.8, 8)
    expfit.FixParameter(0, 0)
    histo.Fit("expfit", "LR")
    fitR = histo.GetFunction("expfit")

    tau = -1/fitR.GetParameter(1)
    tau_s = tau/(3*10**10)  # convert to seconds
    error = tau_s / (8.96*10**-11)  # calculared time divided by known value
    print(tau, tau_s, error)
