#!/usr/bin/python


from ROOT import TString, TFile, TH1D, gDirectory, TGraph
import math
import fits
import canvases


numToAnalyse = 0  # Number of tracks to analyse

# Process for opening a file and getting the data
fileName = TString("AnalysisResults_258477.root")
f = TFile.Open(fileName.Data(), "read")
f.cd("filterV0s")
tree = gDirectory.Get("fTreeOutput")  # Get TTree
numberOfTracks = tree.GetEntriesFast()  # Get number of entries

print("Total number of V0s in file: %d" % (numberOfTracks))
print("Number of V0s to be analysed: %d" % (numToAnalyse))

m_pi = 0.1396  # mass of pi+ and pi- in units of GeV/c^2
m_p = 0.938  # mass of proton in units of GeV/c^2

# Histogram for masses
histM = TH1D("histM", "V0s M", 1000, 0, 5)
# Histogram for masses but zoomed
histMReduced = TH1D("histMReduced", "V0s M", 1000, 0.35, 0.7)
# Histogram for angles
histCos = TH1D("histCos", "$cos(\theta)$", 1000, 0.98, 1.0)
# histogram for life times
histT = TH1D("histT", "exp -t", 1000, 0, 10)

graphdEdx = TGraph()  # define TGraph to hold scatter plot of dE/dx


for count, v0 in enumerate(tree):
    if count >= numToAnalyse and numToAnalyse > 0:
        break

    if count % 1000 == 0:
        print(" --- %d V0s analysed --- " % count)

    pv_particles = v0.pv_particles  # number of particles from primary vertex

    # Primary vertex
    pv_x = v0.pv_x  # pv x-coordinate
    pv_y = v0.pv_y  # pv y-coordinate
    pv_z = v0.pv_z  # pv y-coordinate

    # Secondary vertex
    sv_x = v0.sv_x  # SV (secondary vertex / decay vertex) x-coordinate
    sv_y = v0.sv_y  # SV y-coordinate
    sv_z = v0.sv_z  # SV z-coordinate

    # V0 candidate
    v0_px = v0.v0_px  # V0 x-component
    v0_py = v0.v0_py  # V0 y-component
    v0_pz = v0.v0_pz  # V0 z-component
    v0_p = math.sqrt(v0_px**2 + v0_py**2 + v0_pz**2)  # total momentum

    # First prong particle (daughter) of V0 candidate
    Prong1_charge = v0.prong1_charge  # charge
    Prong1_px = v0.prong1_px  # x-component
    Prong1_py = v0.prong1_py  # y-component
    Prong1_pz = v0.prong1_pz  # z-component
    Prong1_p = math.sqrt(Prong1_px**2 + Prong1_py**2 + Prong1_pz**2)  # momentum
    dEdx1 = v0.prong1_TPCdEdx  # dE/dx

    Prong2_charge = v0.prong2_charge  # charge
    Prong2_px = v0.prong2_px  # x-component
    Prong2_py = v0.prong2_py  # y-component
    Prong2_pz = v0.prong2_pz  # z-component
    Prong2_p = math.sqrt(Prong2_px**2 + Prong2_py**2 + Prong2_pz**2)  # momentum
    dEdx2 = v0.prong2_TPCdEdx  # dE/dx

    Prong_p = math.sqrt( (Prong1_px + Prong2_px)**2 + (Prong1_py + Prong2_py)**2 + (Prong1_pz + Prong2_pz)**2 ) # Momentum of combined prong1 and prong2

    E = math.sqrt(m_pi**2 + Prong1_p**2) + math.sqrt(m_pi**2 + Prong2_p**2)  # Energy
    mass = math.sqrt(E**2 - Prong_p**2)  # invariant mass

    momentumVector = [Prong1_px + Prong2_px, Prong1_py + Prong2_py, Prong1_pz + Prong2_pz]  # combined momentum vector for prongs
    vertexVector = [sv_x - pv_x, sv_y - pv_y, sv_z - pv_z]  # vector going through the 2 vertices

    dotP = momentumVector[0]*vertexVector[0]+momentumVector[1]*vertexVector[1]+momentumVector[2]*vertexVector[2]
    norm = math.sqrt(momentumVector[0]**2 + momentumVector[1]**2 + momentumVector[2]**2)*math.sqrt(vertexVector[0]**2+vertexVector[1]**2 + vertexVector[2]**2)
    angle = dotP/norm

    histCos.Fill(angle)  # Fill histogram with angles
    histM.Fill(mass)  # Fill histogram with invariant masses

    graphdEdx.SetPoint(count, Prong1_p, dEdx1)  # First part of dE/dx plot
    graphdEdx.SetPoint(count + 1, Prong2_p, dEdx2)  # Second part of dE/dx plot

    # Cut point based on their dE/dx for a given momentum
    momentumCut = 0.25  # First choice of where to cut of momenta
    dEdxCut = 70  # The cutof value

    if (Prong1_p > momentumCut and dEdx1 > dEdxCut) or (Prong2_p > momentumCut and dEdx2 > dEdxCut):
        continue

    momentumCut = 0.075  # Second choice
    dEdxCut = 100  # ...

    # if must be refactored..
    if (Prong1_p < momentumCut and dEdx1 < dEdxCut) or (Prong2_p < momentumCut and dEdx2 < dEdxCut):
        continue

    if angle < 0.999:
        continue

    histMReduced.Fill(mass)  # Fill histogram with remaining invariant masses

    # only consider the "top" for fitting lifetimes
    if mass < 0.485 or mass > 0.51:
        continue

    gamma = math.sqrt(1+(Prong_p/mass)**2)
    distance = math.sqrt(vertexVector[0]**2 + vertexVector[1]**2 + vertexVector[2]**2)
    speed = Prong_p / mass
    t = distance/speed
    histT.Fill(t)


fits.mass_fit(histMReduced)
fits.lifetime_fit(histT)

canvases.draw(histM, histMReduced, histCos, histT, graphdEdx)

input("lol")
