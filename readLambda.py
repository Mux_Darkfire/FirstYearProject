#!/usr/bin/python

#### Question?
# why mix camelCase, PascalCase and snake_case???

from ROOT import TString, TFile, TH1D, TH2D, gDirectory, kRed, TCanvas, TGraph, TF1
import math


numToAnalyse = 0 # Number of tracks to analyse

## Process for opening a file and getting the data
fileName = TString("AnalysisResults_258477.root")
f = TFile.Open(fileName.Data(), "read")
f.cd("filterV0s")
tree = gDirectory.Get("fTreeOutput")  # Get TTree
numberOfTracks = tree.GetEntriesFast()  # Get number of entries

print("Total number of V0s in file: %d" % (numberOfTracks))
print("Number of V0s to be analysed: %d" % (numToAnalyse))

m_pi = 0.1396 # mass of pi+ and pi- in units of GeV/c^2
m_p = 0.938 # mass of proton in units of GeV/c^2

histM = TH1D("histM","V0s M",1000, 0, 5) # Histogram for masses with our calculation
histMReduced = TH1D("histMReduced","V0s M",1000, m_pi + m_p, 1.15) # Histogram for masses but zoomed
histCos = TH1D("histCos","$cos(\theta)$", 1000, 0.98, 1.0) # Histogram for angles
histT = TH1D("histT", "exp -t", 1000, 0, 50) # histogram for life times

graphdEdx = TGraph() # define TGraph to hold scatter plot of dE/dx


for count, v0 in enumerate(tree):
    if count >= numToAnalyse and numToAnalyse > 0:
        break

    if count % 1000 == 0:
        print(" --- %d V0s analysed --- " % count)

    pv_particles = v0.pv_particles  # number of particles from primary vertex

    ## Primary vertex
    pv_x = v0.pv_x  # pv x-coordinate
    pv_y = v0.pv_y  # pv y-coordinate
    pv_z = v0.pv_z  # pv y-coordinate

    ## Secondary vertex
    sv_x = v0.sv_x # SV (secondary vertex / decay vertex) x-coordinate
    sv_y = v0.sv_y # SV y-coordinate
    sv_z = v0.sv_z # SV z-coordinate

    ## V0 candidate
    v0_px = v0.v0_px # V0 x-component
    v0_py = v0.v0_py # V0 y-component
    v0_pz = v0.v0_pz # V0 z-component
    v0_p = math.sqrt(v0_px**2 + v0_py**2 + v0_pz**2) # total momentum

    # First prong particle (daughter) of V0 candidate
    Prong1_charge = v0.prong1_charge # charge
    Prong1_px = v0.prong1_px # x-component
    Prong1_py = v0.prong1_py # y-component
    Prong1_pz = v0.prong1_pz # z-component
    Prong1_p = math.sqrt(Prong1_px**2 + Prong1_py**2 + Prong1_pz**2) # total momentum
    dEdx1 = v0.prong1_TPCdEdx # dE/dx

    Prong2_charge = v0.prong2_charge # charge
    Prong2_px = v0.prong2_px # x-component
    Prong2_py = v0.prong2_py # y-component
    Prong2_pz = v0.prong2_pz # z-component
    Prong2_p = math.sqrt(Prong2_px**2 + Prong2_py**2 + Prong2_pz**2) # total momentum
    dEdx2 = v0.prong2_TPCdEdx # dE/dx

    Prong_p = math.sqrt( (Prong1_px + Prong2_px)**2 + (Prong1_py + Prong2_py)**2 + (Prong1_pz + Prong2_pz)**2 ) # Momentum of combined prong1 and prong2

    E = math.sqrt(m_p**2 + Prong1_p**2) + math.sqrt(m_pi**2 + Prong2_p**2) # Energy
    mass = math.sqrt(E**2 - Prong_p**2) # invariant mass

    momentumVector = [Prong1_px + Prong2_px, Prong1_py + Prong2_py, Prong1_pz + Prong2_pz] # combined momentum vector for prongs
    vertexVector = [sv_x - pv_x, sv_y - pv_y, sv_z - pv_z] # vector going through the 2 vertices



    # calculating angles
    dotP = momentumVector[0]*vertexVector[0]+momentumVector[1]*vertexVector[1]+momentumVector[2]*vertexVector[2]
    norm = math.sqrt(momentumVector[0]**2 + momentumVector[1]**2 + momentumVector[2]**2)*math.sqrt(vertexVector[0]**2+vertexVector[1]**2 + vertexVector[2]**2)
    angle = dotP/norm

    histCos.Fill(angle) # Fill histogram with angles
    histM.Fill(mass) #Fill histogram with invariant mass



    # if count < 200000: # number may be subject to change
        # We get too many points to see anything and the file gets huge
    graphdEdx.SetPoint(count, Prong1_p, dEdx1) # First part of dE/dx plot
    graphdEdx.SetPoint(count + 1, Prong2_p, dEdx2) # Second part of dE/dx plot

    # Cut point based on their dE/dx for a given momentum
    

    momentumCutn = 0.125# Second choice
    dEdxCut = 220  # ...
    momentumCutp = 0.19
    # if must be refactored..
    if (momentumCutp > Prong1_p > momentumCutn and dEdx1 > dEdxCut) or (momentumCutp > Prong2_p > momentumCutn and dEdx2 > dEdxCut):
        continue

    momentumCutp = 0.300
    dEdxCutp = 210
    momentumCutm = 0.15
    dEdxCutm = 130

    if (momentumCutp > Prong1_p > momentumCutm and dEdxCutp > dEdx1 > dEdxCutm) or (momentumCutp > Prong2_p > momentumCutm and dEdxCutp > dEdx2 > dEdxCutm):
        continue

    momentumCut = 0.35 
    dEdxCut = 1100 

    if (Prong1_p > momentumCut and dEdx1 > dEdxCut) or (Prong2_p > momentumCut and dEdx2 > dEdxCut):
        continue

    momentumCut = 0.55 
    dEdxCut = 215 

    if (Prong1_p > momentumCut and dEdx1 > dEdxCut) or (Prong2_p > momentumCut and dEdx2 > dEdxCut):
        continue
    

    momentumCut = 0.45 
    dEdxCut = 360 

    if (Prong1_p > momentumCut and dEdx1 > dEdxCut) or (Prong2_p > momentumCut and dEdx2 > dEdxCut):
        continue

    
    if angle < 0.999:
        continue


    # only consider the "top" for fitting
    #if mass < 1 or mass > 1.12:
    #    continue

    histMReduced.Fill(mass) # Fill histogram with remaining invariant masses

    gamma = math.sqrt(1+(Prong_p/1.1)**2)
    distance = math.sqrt(vertexVector[0]**2+vertexVector[1]**2 + vertexVector[2]**2)
    speed = math.sqrt(momentumVector[0]**2 + momentumVector[1]**2 + momentumVector[2]**2)/ mass
    t = (distance/speed)
    histT.Fill(t)


 #Fitting
# expo(0) is apparently a function for fitting exponentials ( exp([0] + x*[1] )
# The output will be 1/tau according to Vojtech
# parameter 0 is fixed to 0. It seemed to provide better results
# fitting is limited to range 2-25, because that seems to be where the distribution is
expfit = TF1("expfit", "expo(0)", 0.1 , 10)
expfit.FixParameter(0,0)
histT.Fit("expfit", "LR")
fitR = histT.GetFunction("expfit")

tau = -1/fitR.GetParameter(1)
tau_s = tau/(3*10**10) # convert to seconds
error = tau_s / (8.96*10**-11) # calculared time divided by known value
print(tau, tau_s, error)


# Create canvas to draw on
# Plotting should be refactored to one function
canMass = TCanvas("canMass")
canMass.SetLogy()
histM.Draw()
canMass.Update()
canMass.SaveAs("mass_plot.pdf")

canMassReduced = TCanvas("canMassReduced")
canMassReduced.SetLogy()
histMReduced.Draw()
histMReduced.GetXaxis().SetTitle("masse[Gev/c^2]");
canMassReduced.Update()
canMassReduced.SaveAs("mass_plot_reduced.pdf")


canCos = TCanvas("canCos")
canCos.SetLogy()
histCos.Draw()
canCos.Update()
canCos.SaveAs("angles.pdf")

canT = TCanvas("canT")
histT.Draw()
canT.Update()
canT.SaveAs("lifetimes.pdf")

candEdx = TCanvas("dEdx")
#candEdx.SetLogy()
graphdEdx.Draw("AP")
graphdEdx.GetXaxis().SetTitle("P[Gev/c^2]")
graphdEdx.GetYaxis().SetTitle("dEdx")
graphdEdx.GetXaxis().SetRangeUser(0,2)
graphdEdx.SetMarkerStyle(20)
graphdEdx.SetMarkerSize(0.1)
candEdx.Update()
candEdx.SaveAs("dEdx.pdf")


input("lol")
